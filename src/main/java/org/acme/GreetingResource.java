package org.acme;

import org.acme.entity.UserEntity;
import org.acme.response.MyRecord;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {

    @PUT
    @Path("hello")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(MyRecord<UserEntity> myRecord) {
        return "Hello "+myRecord.content();
    }
}