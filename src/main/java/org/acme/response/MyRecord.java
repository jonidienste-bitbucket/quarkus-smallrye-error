package org.acme.response;

public record MyRecord<T>(
        T content
) {}
